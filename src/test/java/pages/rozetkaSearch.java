package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.codeborne.selenide.Selenide.$;
import static org.assertj.core.api.Assertions.assertThat;

public class rozetkaSearch extends basePage {

    //Search field
    @FindBy(xpath = "//*[@class='rz-header-search-input-text passive']")
    private WebElement searchField;

    //Search button
    @FindBy(xpath = "//*[@name='rz-search-button']")
    private WebElement searchBtn;

    @FindBy(xpath = "//*[@id='search_result_title_text']")
    private WebElement title;

    public rozetkaSearch(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void rozetkaSearch(String good) {
        $(searchField).sendKeys(good);
        $(searchBtn).click();
        assertThat($(title).getText().equalsIgnoreCase(good));
    }

}

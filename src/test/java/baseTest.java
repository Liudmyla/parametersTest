import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import utils.config;

import java.util.concurrent.TimeUnit;

abstract class baseTest {

    WebDriver driver;

    private final String url = config.getProperty("baseUrl");
    private final String browser = config.getProperty("driver");
    private final String timeout = config.getProperty("timeout");

    private WebDriver getDriver() {
        if (browser.equals("chrome")) return new ChromeDriver();
        if (browser.equals("firefox")) return new FirefoxDriver();
        return new ChromeDriver();
    }

    @Before
    public void setupTest() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = getDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(Integer.parseInt(timeout), TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(Integer.parseInt(timeout), TimeUnit.SECONDS); // устанавливаем таймаут для загрузки скриптов
        driver.manage().timeouts().pageLoadTimeout(Integer.parseInt(timeout), TimeUnit.SECONDS);// устанавливаем таймаут для загрузки страницы
        driver.get(url);
    }

    @After
    public void Termination() {
        driver.close();
        driver.quit();
    }
}

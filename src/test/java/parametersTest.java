import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;
import pages.rozetkaSearch;

@RunWith(JUnitParamsRunner.class)
public class parametersTest extends baseTest {

    public Object[] goodItems() {
        return new Object[]{
                "Кофе",
                "Книги для бизнеса",
                "Путешествия"
        };
    }

    @Test
    @Parameters(method = "goodItems")
    public void goodsSearch(String good) {
        System.out.println("TEST: Goods search");
        rozetkaSearch mainSearch = new rozetkaSearch(driver);
        mainSearch.rozetkaSearch(good);
    }

 }

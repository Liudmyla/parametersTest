import org.junit.Test;
import pages.rozetkaMain;

public class allTests extends baseTest {

    @Test
    public void homeGoodsTest() {
        System.out.println("TEST: Tovary dlia doma");
        rozetkaMain mainpage = new rozetkaMain(driver);

        //tovary dlia doma
        mainpage.homeGoodsOpen();
        System.out.println("Web element found: tovary dlia doma");

        //click element "Vse kategorii" in "tovary dlia doma" section
        mainpage.allCategoriesOpen();
        System.out.println("Web element found: all categories");
    }

    @Test
    public void smartphoneTest() {
        System.out.println("TEST: Smartphone");
        rozetkaMain mainpage = new rozetkaMain(driver);

        //click element "Smartphones, TV, electronics"
        mainpage.smartphonesOpen();
        System.out.println("Web element found: Smartphones, TV, electronics");

        //click element Smartphones in ""Smartphones, TV.." section
        mainpage.smartphonesAllOpen();
        System.out.println("Web element found: Smartphones");

        //write smartphoneList to console and file
        mainpage.findSmartphoneList();
    }

    @Test
    public void washPowders() {
        rozetkaMain mainpage = new rozetkaMain(driver);
        //tovary dlia doma
        mainpage.homeGoodsOpen();
        //bytovaia himia
        mainpage.chemicalsOpen();
        //sredstva dlia stirki
        mainpage.forLaunderOpen();
        //stir poroshki
        mainpage.washPowderOpen();
        //copy list to file
        mainpage.findWashPowderList();
    }
}
